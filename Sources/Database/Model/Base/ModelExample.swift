//
//  ModelExample.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 5/18/18.
//

import Foundation

import Realm
import RealmSwift

import Foundation

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

class ModelExample: Model, Mockable, PropertyListable {
    
    // Set your DB properties here
    @objc dynamic var mandatoryProperty:String = ""
    @objc dynamic var optionalProperty:String = ""
    
    // Implement required init
    required init?(from request: HTTPRequest) {
        guard let _mandatoryProperty = request.param(name: "mandatoryProperty") else { return nil }
        super.init()
        
        assignID(from: request)
        
        mandatoryProperty = _mandatoryProperty
        optionalProperty = request.param(name: "optionalProperty") ?? ""
    }
    
    // ----------- Don't edit this section - just copy-paste to your model class ----------- //
    //MARK: - Protocol conformance -
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    static func propertyNames() -> [String] {
        return Mirror.propertyNames(for: self.init())
    }
}
