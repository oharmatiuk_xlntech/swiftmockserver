//
//  Model.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 5/1/18.
//

import Foundation
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import RealmSwift
import Realm

protocol Mockable:JSONConvertible {
    init?(from request:HTTPRequest)
}

protocol PropertyListable {
    static func propertyNames() -> [String]
}

class Model:Object {
    static var apiPath = "/api/models"
    static var availableModels:[Model.Type] = [ModelExample.self] // <--- Add your custom models here as: [ModelExample.self, ...]
    
    @objc dynamic var id:Int = -1
    
    class var uri:String { get { return "\(apiPath)/\(String(describing:self).lowercased())" }}
    
    class var routes:[Route] {
        get {
            return [createRoute, getRoute, deleteRoute]
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func assignID(from request: HTTPRequest) {
        if let existingID = Int(request.param(name: "id") ?? "") {
            id = existingID
        } else {
            id = try! Realm().objects(type(of: self)).count + 1
        }
    }
    
    // MARK: - Protocols conformace -
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    override init(value: Any) {
        super.init(value: value)
    }
    
    func jsonEncodedString() throws -> String {
        var dictionary = [String:Any]()
        
        guard let listableSelf = self as? PropertyListable else {
            throw NSError(domain: "", code: 1, userInfo: nil)
        }
        
        let propertyNames = type(of:listableSelf).propertyNames()
        for propertyName in propertyNames {
            dictionary[propertyName] = value(forKey: propertyName)
        }
        return try! dictionary.jsonEncodedString()
    }
}


