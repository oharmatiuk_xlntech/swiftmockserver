//
//  Model+CRUD.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 5/15/18.
//

import Realm
import RealmSwift

import PerfectHTTP
import PerfectHTTPServer


extension Model {
    // CRUD endpoints
    class var createRoute: Route {
        get {
            return Route(method: .post, uri: uri, handler: { (request, response) in
                guard let listableSelf = self as? PropertyListable.Type else {
                    response.completed(status: .internalServerError)
                    return
                }
                guard let mockableSelf = self as? Mockable.Type else {
                    response.completed(status: .internalServerError)
                    return
                }
                guard let object = mockableSelf.init(from: request) as? Model else {
                    let status:RequestValidationStatus = request.validateParamaters(for: listableSelf)
                    if let validationErrorMessage = status.message {
                        response.setBody(string: validationErrorMessage)
                    } else {
                        response.setBody(string: "Failed to create instance of \(mockableSelf). Check parameter values")
                    }
                    response.completed(status: .badRequest)
                    return
                }
                
                let realm = try! Realm()
                try! realm.write {
                    realm.add(object, update: true)
                }
                
                print("\(listableSelf) created: \(object).")
                print("Entries of \(mockableSelf) in database: \(realm.objects(mockableSelf as! Model.Type).count)")
                response.completed()
            })
        }
    }
    
    class var getRoute: Route {
        get {
            return Route(method: .get, uri: uri, handler: { (request, response) in
                guard let mockableSelf = self as? Mockable.Type else {
                    return
                }
                do {
                    let responseJSON = try Array(try Realm().objects(mockableSelf as! Model.Type)).jsonEncodedString()
                    response.appendBody(string: responseJSON)
                    response.completed()
                } catch let error {
                    response.setBody(string: "Failed to get instances of \(mockableSelf). Error: \(error.localizedDescription)")
                    response.completed(status: .internalServerError)
                    return
                }
            })
        }
    }
    
    class var deleteRoute: Route {
        get {
            return Route(method: .delete, uri: uri, handler: { (request, response) in
                // Fetch person with ID from request and delete it
                guard let listableSelf = self as? PropertyListable.Type else {
                    return
                }
                let realm = try! Realm()
                let objects = realm.objects(listableSelf as! Model.Type)
                try! realm.write {
                    realm.delete(objects)
                }
                print("\(listableSelf) deleted")
                response.completed()
            })
        }
    }
}
