//
//  Routes.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 4/26/18.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer

class URLRoutes {
    static var routes:Routes = {
        let availableRoutes = Model.availableModels.flatMap({ return $0.routes })
        return Routes(availableRoutes)
    }()
}
