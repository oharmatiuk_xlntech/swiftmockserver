//
//  Server.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 4/26/18.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer
import Realm
import RealmSwift

class Server {
    static let shared = Server()
    
    private let server:HTTPServer.Server = {
        return HTTPServer.Server(name: "Mock_Server", address: "0.0.0.0", port: 8181, routes: URLRoutes.routes)
    }()
    
    func launch() {
        assert(Model.availableModels.count > 0, "Models can't be empty. Please, add supported model classes")
        do {
            Realm.Configuration.defaultConfiguration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
            try HTTPServer.launch([server])
        } catch let error {
            debugPrint(error)
        }
    }
}
