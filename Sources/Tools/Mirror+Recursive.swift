//
//  Mirror+Recursive.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 5/7/18.
//

import Foundation

extension Mirror {
    static func propertyNames(for instance: PropertyListable) -> [String] {
        let mirror = Mirror(reflecting: instance)
        return mirror.propertyNames()
    }
    
    private func propertyNames() -> [String] {
        var propertyNames = [String]()
        
        // Properties of this instance:
        propertyNames.append(contentsOf: children.map { $0.label ?? "N/A" })
        
        // Add properties of superclass:
        if let parent = self.superclassMirror {
            propertyNames.append(contentsOf: parent.propertyNames())
        }
        
        return propertyNames
    }
    
}
