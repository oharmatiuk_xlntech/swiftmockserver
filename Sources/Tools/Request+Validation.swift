//
//  Request+Tools.swift
//  SwiftServerTest
//
//  Created by O.Harmatiuk on 5/8/18.
//

import Foundation
import PerfectHTTP
import PerfectHTTPServer

enum RequestValidationStatus {
    case passed
    case failed(String, PropertyListable.Type)
    
    var message:String? {
        get {
            switch self {
            case .passed:
                return nil
            case .failed(let paramName, let type):
                return "Params validation failed: \"\(paramName)\" is required for entity \"\(type)\""
            }
        }
    }
}

extension HTTPRequest {
    func validateParamaters(for type: PropertyListable.Type) -> RequestValidationStatus {
        for propertyName in type.propertyNames() {
            if propertyName.lowercased() == "id" { continue } // Don't validate param "ID" - it's added by server
            
            guard let _ = param(name: propertyName) else {
                return .failed(propertyName, type)
            }
        }
        return .passed
    }
}
